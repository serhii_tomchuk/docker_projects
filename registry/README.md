```
cat TOKEN.txt | docker login registry.gitlab.com/ -u {USER_NAME} --password-stdin
```

```
docker build -t registry.gitlab.com/{USER_NAME}/{REPO_NAME}/{IMAGE_NAME}:latest .
```

```
docker push registry.gitlab.com/{USER_NAME}/{REPO_NAME}/{IMAGE_NAME}:latest
```